import React, { Component } from 'react';
import './App.css';
import ImageSlider from './components/ImageSlider';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ImageSlider/>
      </div>
    );
  }
}

export default App;
