import types from './types';

export const previous = (id) => {
  return { type: types.PREVIOUS, id: id };
};

export const next = (id) => {
  return { type: types.NEXT, id: id };
};

