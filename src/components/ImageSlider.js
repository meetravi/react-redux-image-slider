import React from 'react';
import { connect } from 'react-redux';
import { previous, next } from './../action/creators';
import './styles.css';

export class ImageSlider extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      isPreviousDisabled: true,
      isNextDisabled: false,
    };
    this.nextItem = this.nextItem.bind(this);
    this.previousItem = this.previousItem.bind(this);
    this.imageRef = React.createRef();
  }

  nextItem = () => {
    const { next, images } = this.props;
    const isLastItem = (parseInt(this.imageRef.current.getAttribute('data-index'), 10) + 1) === images.length;
    if (isLastItem) {
      this.setState({
        isPreviousDisabled: false,
        isNextDisabled: true,
      });
    } else {
      this.setState({
        isPreviousDisabled: false,
        isNextDisabled: false,
      });
    }
    next();
  }

  previousItem = () => {
    const { previous } = this.props;
    const isFirstItem = (parseInt(this.imageRef.current.getAttribute('data-index'), 10) - 1) === 1;
    if (isFirstItem) {
      this.setState({
        isPreviousDisabled: true,
        isNextDisabled: false,
      });
    } else {
      this.setState({
        isPreviousDisabled: false,
        isNextDisabled: false,
      });
    }
    previous();
  }

  render(){
    const { images } = this.props;
    const { isPreviousDisabled, isNextDisabled } = this.state;
    return (
      <div>
        {
          images.map((image, index) => {
            return image.visible ? <img ref={this.imageRef} key={image.id} data-index={image.id} src={image.src} alt={image.text} data-visible={image.visible}/>:'';
          })
        }
        <br />
        <button className='previous' onClick={this.previousItem} disabled={isPreviousDisabled}>prev</button>
        <button className='next' onClick={this.nextItem} disabled={isNextDisabled}>next</button>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return { images: state.app.images };
};

const mapDispatchToProps = (dispatch) => ({
  previous: () => dispatch(previous()),
  next: () => dispatch(next()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageSlider);
