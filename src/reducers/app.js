import types from './../action/types';

export const initialState = {
  images: [
    { src: 'http://via.placeholder.com/350x150?text=1', text: 'one', visible: true, id:1},
    { src: 'http://via.placeholder.com/350x150?text=2', text: 'two', visible: false, id:2},
    { src: 'http://via.placeholder.com/350x150?text=3', text: 'three', visible: false, id:3},
  ],
};

const navigateImage = (state, direction) => {
  let visibleIndex = state.images.findIndex(image => image.visible);
  if (direction === 'next') {
    visibleIndex = visibleIndex + 1;
  } else {
    visibleIndex = visibleIndex - 1;
  }
  const images = state.images.map((image, index) => {
    if (visibleIndex === index) {
      return {...image, visible:true}
    }
    return {...image, visible: false}
  });
  return images;
}

const app = (state = initialState, actions) => {
  switch (actions.type) {
    case types.PREVIOUS:
      return {
        images: navigateImage(state, 'previous')
      }
    case types.NEXT:
      return {
        images: navigateImage(state, 'next')
      }
    default:
      return state
    }
}

export default app