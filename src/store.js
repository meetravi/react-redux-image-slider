import { createStore } from 'redux'

import reducers from './reducers'
import middleware from './middleware'

const initialState = {};

const store = createStore(
  reducers,
  initialState,
  middleware,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

export {
  store
}